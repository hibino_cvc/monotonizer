/* Gloval Variables */
var lStorage = {
  mode: "imageonly",
  preset: "grayscale",
  curSettings: {
    grayscale: "100",
    sepia: "0",
    hue: "0",
    invert: "0",
    bright: "0",
    contrast: "0",
    saturate: "0",
    blur: "0",
    opacity: "0"
  }
};
var PRESETS = {
  original:  {grayscale:  "0",sepia:  "0",hue:  "0",invert:  "0",bright:"100",contrast:"100",saturate:"100",blur:  "0",opacity:"100"},
  grayscale: {grayscale:"100",sepia:  "0",hue:  "0",invert:  "0",bright:"100",contrast:"100",saturate:"100",blur:  "0",opacity:"100"},
  sepia:     {grayscale:  "0",sepia:"100",hue:  "0",invert:  "0",bright:"100",contrast:"100",saturate:"100",blur:  "0",opacity:"100"},
  blur:      {grayscale:  "0",sepia:  "0",hue:  "0",invert:  "0",bright:"100",contrast:"100",saturate:"100",blur:  "1",opacity:"100"},
  blurgray:  {grayscale:"100",sepia:  "0",hue:  "0",invert:  "0",bright:"100",contrast:"100",saturate:"100",blur:  "1",opacity:"100"},
  blursepia: {grayscale:  "0",sepia:"100",hue:  "0",invert:  "0",bright:"100",contrast:"100",saturate:"100",blur:  "1",opacity:"100"}
}



/* Settings保存処理 */
function saveSettings() {
  lStorage.mode = $('input[name="rdMode"]:checked').val();
  lStorage.preset = $('input[name="rdPreset"]:checked').val();

  lStorage.curSettings.grayscale = $('#txGrayscale').val();
  lStorage.curSettings.sepia = $('#txSepia').val();
  lStorage.curSettings.hue = $('#txHue').val();
  lStorage.curSettings.invert = $('#txInvert').val();
  lStorage.curSettings.bright = $('#txBright').val();
  lStorage.curSettings.contrast = $('#txContrast').val();
  lStorage.curSettings.saturate = $('#txSaturate').val();
  lStorage.curSettings.blur = $('#txBlur').val();
  lStorage.curSettings.opacity = $('#txOpacity').val();

  chrome.storage.local.set(lStorage, function(){});

  $("#btnSave").prop("disabled", true);
}

/* Settings読込処理 */
function loadSettings() {
  chrome.storage.local.get(lStorage, function(storage) {

    if (null != storage) {
      lStorage = storage;
    } else {
      lStorage = {
        mode: "imageonly",
        preset: "grayscale",
        curSettings: {
          grayscale: "100",
          sepia: "0",
          hue: "0",
          invert: "0",
          bright: "0",
          contrast: "0",
          saturate: "0",
          blur: "0",
          opacity: "0"
        }
      };
    }
    $('input[value="' + lStorage.mode + '"]').prop('checked', true);
    $('input[value="' + lStorage.preset + '"]').prop('checked', true);
    
    $('#txGrayscale').val(lStorage.curSettings.grayscale);
    $('#txSepia').val(lStorage.curSettings.sepia);
    $('#txHue').val(lStorage.curSettings.hue);
    $('#txInvert').val(lStorage.curSettings.invert);
    $('#txBright').val(lStorage.curSettings.bright);
    $('#txContrast').val(lStorage.curSettings.contrast);
    $('#txSaturate').val(lStorage.curSettings.saturate);
    $('#txBlur').val(lStorage.curSettings.blur);
    $('#txOpacity').val(lStorage.curSettings.opacity);
    
    $('#rgGrayscale').val(lStorage.curSettings.grayscale);
    $('#rgSepia').val(lStorage.curSettings.sepia);
    $('#rgHue').val(lStorage.curSettings.hue);
    $('#rgInvert').val(lStorage.curSettings.invert);
    $('#rgBright').val(lStorage.curSettings.bright);
    $('#rgContrast').val(lStorage.curSettings.contrast);
    $('#rgSaturate').val(lStorage.curSettings.saturate);
    $('#rgBlur').val(lStorage.curSettings.blur);
    $('#rgOpacity').val(lStorage.curSettings.opacity);

    drawImage();
    $("#btnSave").prop("disabled", true);
  });
}

/* 画像へのフィルタ反映 */
function drawImage() {
  var vMode = $('input[name="rdMode"]:checked').val();

  var filter = {};
  filter.grayscale = $('#txGrayscale').val();
  filter.sepia = $('#txSepia').val();
  filter.hue = $("#txHue").val();
  filter.invert = $("#txInvert").val();
  filter.bright = $("#txBright").val();
  filter.contrast = $("#txContrast").val();
  filter.saturate = $("#txSaturate").val();
  filter.blur = $("#txBlur").val();
  filter.opacity = $("#txOpacity").val();

  var vFilter = 'grayscale(' + filter.grayscale + '%) '
      + 'sepia(' + filter.sepia + '%) '
      + 'hue-rotate(' + filter.hue + 'deg) '
      + 'invert(' + filter.invert + '%) '
      + 'brightness(' + filter.bright + '%) '
      + 'contrast(' + filter.contrast + '%) '
      + 'saturate(' + filter.saturate + '%) '
      + 'blur(' + filter.blur + 'px) '
      + 'opacity(' + filter.opacity + '%) ';

  if ("imageonly" == vMode) {
    $('#sample1,#sample2').css('-webkit-filter', vFilter);
    $('#sampletext').css('-webkit-filter', "");
  } else if ("fullpage" == vMode) {
    $('#sample1,#sample2,#sampletext').css('-webkit-filter', vFilter);
  }

  // check presets
  var bFind = false;
  for (var key in PRESETS) {
    if (filter.grayscale == PRESETS[key]['grayscale'] &&
        filter.sepia == PRESETS[key]['sepia'] &&
        filter.hue == PRESETS[key]['hue'] &&
        filter.invert == PRESETS[key]['invert'] &&
        filter.bright == PRESETS[key]['bright'] &&
        filter.contrast == PRESETS[key]['contrast'] &&
        filter.saturate == PRESETS[key]['saturate'] &&
        filter.blur == PRESETS[key]['blur'] &&
        filter.opacity == PRESETS[key]['opacity']
    ) {
      $('input[value="' + key + '"]').prop('checked', true);
      bFind = true;
      break;
    }
  }
  if (!bFind) {
    $('input[value="custom"]').prop('checked', true);
  }

}

$(document).ready(function () {

  /* 入力系イベントハンドラ */
  $('input').on( 'input', function () {
    var val = $(this).val();
    var nm = this.id;
    switch (nm) {
      case "rgGrayscale":
        $('#txGrayscale').val(val);
        break;
      case "txGrayscale":
        $('#rgGrayscale').val(val);
        break;
      case "rgSepia":
        $("#txSepia").val(val);
        break;
      case "txSepia":
        $("#rgSepia").val(val);
        break;
      case "rgHue":
        $("#txHue").val(val);
        break;
      case "txHue":
        $("#rgHue").val(val);
        break;
      case "rgInvert":
        $("#txInvert").val(val);
        break;
      case "txInvert":
        $("#rgInvert").val(val);
        break;
      case "rgBright":
        $("#txBright").val(val);
        break;
      case "txBright":
        $("#rgBright").val(val);
        break;
      case "rgContrast":
        $("#txContrast").val(val);
        break;
      case "txContrast":
        $("#rgContrast").val(val);
        break;
      case "rgSaturate":
        $("#txSaturate").val(val);
        break;
      case "txSaturate":
        $("#rgSaturate").val(val);
        break;
      case "rgBlur":
        $("#txBlur").val(val);
        break;
      case "txBlur":
        $("#rgBlur").val(val);
        break;
      case "rgOpacity":
        $("#txOpacity").val(val);
        break;
      case "txOpacity":
        $("#rgOpacity").val(val);
        break;
      default:
        return;
    }

    drawImage();
    $("#btnSave").prop("disabled", false);

  } );

  /* ボタン系イベントハンドラ */
  $('input').on( 'click', function () {
      var filter = {};
      filter.grayscale = $('#txGrayscale').val();
      filter.sepia = $('#txSepia').val();
      filter.hue = $("#txHue").val();
      filter.invert = $("#txInvert").val();
      filter.bright = $("#txBright").val();
      filter.contrast = $("#txContrast").val();
      filter.saturate = $("#txSaturate").val();
      filter.blur = $("#txBlur").val();
      filter.opacity = $("#txOpacity").val();

      var bDraw=true;
      var nm = this.id;
      switch (nm) {
        case "rdPresetOriginal":
          filter = PRESETS.original;
          break;
        case "btnReset":
          if (!window.confirm("Are you sure you want to reset the setting?")) return;
        case "rdPresetGrayscale":
          filter = PRESETS.grayscale;
          break;
        case "rdPresetSepia":
          filter = PRESETS.sepia;
          break;
        case "rdPresetBlur":
          filter = PRESETS.blur;
          break;
        case "rdPresetBlurGray":
          filter = PRESETS.blurgray;
          break;
        case "rdPresetBlurSepia":
          filter = PRESETS.blursepia;
          break;
        case "rdImageonly":
        case "rdFullpage":
          break;
        default:
          bDraw = false;
      }
      if (bDraw) {
        $('#txGrayscale').val(filter.grayscale);
        $('#txSepia').val(filter.sepia);
        $('#txHue').val(filter.hue);
        $('#txInvert').val(filter.invert);
        $('#txBright').val(filter.bright);
        $('#txContrast').val(filter.contrast);
        $('#txSaturate').val(filter.saturate);
        $('#txBlur').val(filter.blur);
        $('#txOpacity').val(filter.opacity);
        $('#rgGrayscale').val(filter.grayscale);
        $('#rgSepia').val(filter.sepia);
        $('#rgHue').val(filter.hue);
        $('#rgInvert').val(filter.invert);
        $('#rgBright').val(filter.bright);
        $('#rgContrast').val(filter.contrast);
        $('#rgSaturate').val(filter.saturate);
        $('#rgBlur').val(filter.blur);
        $('#rgOpacity').val(filter.opacity);
        drawImage();
        $("#btnSave").prop("disabled", false);
      }

      if ("btnReset" == nm || "btnSave" == nm ) {
        // Settings保存処理
        saveSettings();
      }

  });

  $('#cmdsave').on('click', function () {
    chrome.extension.sendMessage({"command": "clearLoadFlag"}, function (response) {
      ;
    });
    window.close();
  });

  $('#cmdcancel').on('click', function () {
    window.close();
  });

  // 起動時処理
  loadSettings();
  


});

