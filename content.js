
chrome.runtime.onMessage.addListener(
    function(req, sender, callback) {

        if ("run" == req.message) {

            //console.log(req.message);
            //console.log(req.storage);

            var vMode = req.storage.mode;

            var vGrayscale = req.storage.curSettings.grayscale;
            var vSepia = req.storage.curSettings.sepia;
            var vHue = req.storage.curSettings.hue;
            var vInvert = req.storage.curSettings.invert;
            var vBright = req.storage.curSettings.bright;
            var vContrast = req.storage.curSettings.contrast;
            var vSaturate = req.storage.curSettings.saturate;
            var vBlur = req.storage.curSettings.blur;
            var vOpacity = req.storage.curSettings.opacity;
            var vFilter = 'grayscale(' + vGrayscale + '%) '
              + 'sepia(' + vSepia + '%) '
              + 'hue-rotate(' + vHue + 'deg) '
              + 'invert(' + vInvert + '%) '
              + 'brightness(' + vBright + '%) '
              + 'contrast(' + vContrast + '%) '
              + 'saturate(' + vSaturate + '%) '
              + 'blur(' + vBlur + 'px) '
              + 'opacity(' + vOpacity + '%) ';

            if ("imageonly" == vMode) {
                $('img').css('-webkit-filter', vFilter);
            } else if ("fullpage" == vMode) {
                document.documentElement.style.webkitFilter = vFilter;
            }


        } else if ("stop" == req.message) {
            
            //console.log(req.message);
            //console.log(req.storage);

            var vMode = req.storage.mode;
            
            if ("imageonly" == vMode) {
                $('img').css('-webkit-filter', "");
            } else if ("fullpage" == vMode) {
                document.documentElement.style.webkitFilter = "";
            }
        }
    }
);