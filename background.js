/* Gloval Variables */
var running = false;
var lStorage = {
  mode: "imageonly",
  preset: "grayscale",
  curSettings: {
    grayscale: "0",
    sepia: "0",
    hue: "0",
    invert: "0",
    bright: "0",
    contrast: "0",
    saturate: "0",
    blur: "0",
    opacity: "0"
  }
};

chrome.browserAction.onClicked.addListener(function(tab) {

	chrome.storage.local.get(lStorage, function(storage) {

		if (running === false)
		{
			running = true;
			chrome.browserAction.setIcon({path:"monolog_sw.png"});
			chrome.tabs.query({}, function(tabs){
				for (var i = 0; i < tabs.length; i++) {

					console.log("[run]tab" + i + ":" + tabs[i].id);

					chrome.tabs.sendMessage(tabs[i].id, {
						message: "run",
						storage: storage}
					,function(msg) {
						return true;
					});
				}
			});
		}
		else
		{
			running = false;
			chrome.browserAction.setIcon({path:"monolog.png"});
			chrome.tabs.query({}, function(tabs){
				for (var i = 0; i < tabs.length; i++) {

					console.log("[stop]tab" + i + ":" + tabs[i].id);

					chrome.tabs.sendMessage(tabs[i].id, {
						message: "stop",
						storage: storage}
					,function(msg) {
						return true;
					});
				}
			});
		}

	});

});


chrome.tabs.onUpdated.addListener(function() {

	chrome.storage.local.get(lStorage, function(storage) {

		if (running === true)
		{
			chrome.tabs.query({}, function(tabs){
				for (var i = 0; i < tabs.length; i++) {
					chrome.tabs.sendMessage(tabs[i].id, {
						message: "run",
						storage: storage}
					,function(msg) {
						return true;
					});
				}
			});
		}

	});

});

chrome.tabs.onActivated.addListener(function(tabId) {

	chrome.storage.local.get(lStorage, function(storage) {

		if (running === true) {
			chrome.tabs.sendMessage(tabId, {
				message: "run",
				storage: storage}
			,function(msg) {
				return true;
			});
		} else {
			chrome.tabs.sendMessage(tabId, {
				message: "stop",
				storage: storage}
			,function(msg) {
				return true;
			});
		}

	});

});